import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.system.exitProcess

import compiler.Compiler
import compilation_unit.CompilationUnit
import parser.parseTopLevel
import scanner.pretty_scanner.scanPretty

// Config
val llcPath = "/usr/bin/llc"
val lldPath = "/usr/bin/ld.lld"
val stdlibPath = "stdlib"

fun main(args: Array<String>) {
  when (args.getOrNull(0)) {
    "tokens" -> {
      val sources = getSources(args)
      sources.forEach{ (module, source) ->
        println(" === ${module} ===")
        val tokens = lexer.lex(source)
        tokens.forEach{ token ->
          println(token.str)
        }
      }
    }
    "ast" -> {
      val sources = getSources(args)
      sources.forEach { (path, source) ->
        val cu = CompilationUnit(source, path.split("/"))
        println("  === ${cu.modulePath} ===")
        println(scanPretty(cu.topLevel))
      }
    }
    "types" -> {
      val sources = getSources(args)
      val compiler = Compiler(sources)
      println(compiler)
      compiler.compilationUnits.forEach { cu ->
        println("=== ${cu.modulePath}")
        println(cu.localTypes.text())
      }
    }
    "ir" -> {
      val sources = getSources(args)
      val compiler = Compiler(sources)
      println(compiler)
      val dir = Paths.get("rhea.out", "ir")
      compiler.emit(dir)
    }
    "build" -> {
      val sources = getSources(args)
      val compiler = Compiler(sources)
      val irDir = Paths.get("rhea.out", "ir")
      val objDir = Paths.get("rhea.out", "obj")
      Files.createDirectories(irDir)
      Files.createDirectories(objDir)
      val irPaths = compiler.emit(irDir)
      val objPaths: List<Path> = irPaths.map foo@ { filename ->
        val src = Paths.get(irDir.toString(), filename)
        val dest = Paths.get(objDir.toString(), "${filename}.o")
        val proc = Runtime.getRuntime().exec(
          arrayOf(llcPath, "-filetype=obj", src.toString(), "-o", dest.toString())
        )
        val exitCode = proc.waitFor()
        if(exitCode != 0) {
          val buffer = ByteArray(1024)
          val errStream = proc.getErrorStream()
          val errString = buildString{
            append("llc exited with code ", exitCode, ": ")
            var count = errStream.read(buffer)
            while(count > 0) {
              append(buffer.toString(Charsets.UTF_8))
              count = errStream.read(buffer)
            }
          }

          throw Exception(errString)
        }
        return@foo dest
      }
      val exePath = Paths.get("rhea.out", "exe")
      val cmd = arrayOf(lldPath, "-o", exePath.toString()) + objPaths.map{ it.toString() }
      val proc = Runtime.getRuntime().exec(cmd)
      val exitCode = proc.waitFor()
      if(exitCode != 0) {
        val buffer = ByteArray(1024)
        val errStream = proc.getErrorStream()
        val errString = buildString{
          append("llc exited with code ", exitCode, ": ")
          var count = errStream.read(buffer)
          while(count > 0) {
            append(buffer.toString(Charsets.UTF_8))
            count = errStream.read(buffer)
          }
        }

        throw Exception(errString)
      }
    }
    "help" -> usage()
    else -> usage(1)
  }
}

fun usage(exitCode: Int = 0) {
  println("Usage: rhea <tokens|ast|help> <source file>")
  exitProcess(exitCode)
}

fun getSources(args: Array<String>): Map<String, String> {
  if (2 > args.size) {
    println("Expects at least one filename")
    exitProcess(1)
  }
  val ret = mutableMapOf<String, String>()
  val seq = args.asSequence().drop(1)
  seq.forEach { filename ->
    walkSource(Paths.get(filename), ret)
  }
  walkSource(Paths.get(stdlibPath), ret)
  return ret
}

fun walkSource(path: Path, map: MutableMap<String, String>): MutableMap<String, String> {
  val pathStr = path.toString()
  if (Files.isDirectory(path)) {
    val dirStream = Files.newDirectoryStream(path)
    dirStream.forEach { dir ->
      walkSource(dir, map)
    }
  } else {
    val source = Files.readAllBytes(path).toString(Charsets.UTF_8)
    val moduleName = if(pathStr.endsWith(".rh")) { pathStr.substring(0, pathStr.length - 3) } else { pathStr }
    if(map.containsKey(moduleName)) {
      throw Exception("Multiple modules named ${moduleName}")
    }
    map[moduleName] = source
  }
  return map
}

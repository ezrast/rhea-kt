package ast

import kotlin.text.StringBuilder as StringBuilder

interface ASTNode

data class AliasAST(
  val type: TypeAST,
  val alias: TypeAST
) : ASTNode

data class ModuleAST(
  val name: String,
  val aliases: List<AliasAST>,
  val actors: List<ActorAST>,
  val modules: List<ModuleAST>,
  val slices: List<SliceAST>,
  val defs: List<DefAST>,
  val rawDefs: List<RawDefAST>,
  val biTypes: List<BiValueAST>
) : ASTNode

data class ImportAST(
  val path: String,
  val typeNames: List<String>
) : ASTNode

data class BiValueAST(
  val name: String,
  val irName: String,
  val rawDefs: List<RawDefAST>
) : ASTNode

data class ActorAST(
  val name: String,
  val states: List<StateAST>,
  val defs: List<DefAST>,
  val rawDefs: List<RawDefAST>
) : ASTNode

data class SliceAST(
  val name: String,
  val inits: List<InitAST>,
  val defs: List<DefAST>,
  val fin: List<ExpressionAST>
) : ASTNode

data class StateAST(
  val name: String,
  val arguments: List<ArgumentAST>,
  val body: List<ExpressionAST>
) : ASTNode

data class InitAST(
  val name: String,
  val arguments: List<ArgumentAST>,
  val body: List<ExpressionAST>
) : ASTNode

data class DefAST(
  val name: String,
  val arguments: List<ArgumentAST>,
  val type: TypeAST,
  val body: List<ExpressionAST>
) : ASTNode

data class RawDefAST(
  val name: String,
  val arguments: List<ArgumentAST>,
  val type: TypeAST,
  val strings: List<StringLitAST>
) : ASTNode

data class ArgumentAST(
  val name: String,
  val type: TypeAST
) : ASTNode

data class TypeAST(
  val name: String,
  val path: List<String> = listOf<String>()
) : ASTNode

sealed class ExpressionAST : ASTNode {}

data class CallAST(
  val callee: String,
  val namespace: List<String>,
  val arguments: List<ExpressionAST>
) : ExpressionAST() {
  fun text(): String {
    val sb = StringBuilder()
    namespace.forEach{ ns ->
      sb.append('.')
      sb.append(ns)
    }
    sb.append('.')
    sb.append(callee)
    return sb.toString()
  }
}

data class MethodAST(
  val receiver: ExpressionAST,
  val callee: String,
  val arguments: List<ExpressionAST>
) : ExpressionAST()

data class AssignmentAST(
  val lvar: LVarAST,
  val value: ExpressionAST
) : ExpressionAST()

data class LVarAST(
  val name: String
) : ExpressionAST()

data class StringLitAST(
  val value: ByteArray
) : ExpressionAST() {
  fun asString(): String = value.map{ it.toChar() }.joinToString(separator = "")
}

data class IntLitAST(
  val value: Int
) : ExpressionAST() {
  fun irName(): String =
    value.toString()
}

data class LambdaLitAST(
  val arguments: List<ArgumentAST>,
  val retType: TypeAST,
  val body: List<ExpressionAST>
) : ExpressionAST()

data class RetAST(
  val value: ExpressionAST
) : ExpressionAST()

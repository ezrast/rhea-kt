package compilation_unit

import ast.*
import parser.parseTopLevel
import scanner.alias_scanner.scanAliases
import scanner.constant_scanner.scanConstants
import scanner.type_scanner.scanTypes
import type_map.TypeMap

class CompilationUnit(val source: String, val modulePath: List<String>) {
  val topLevel: ModuleAST = parseTopLevel(source)
  val localTypes: TypeMap = scanTypes(topLevel)
  val aliases = scanAliases(topLevel)
  val constants = scanConstants(topLevel)
}

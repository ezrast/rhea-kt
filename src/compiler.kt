package compiler

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

import ast.ImportAST
import ast.TypeAST
import compilation_unit.CompilationUnit
import emitter.emitCompilationUnit
import function_map.FunctionMap
import function_signature.FunctionSignature
import scanner.function_scanner.scanFunctions
import scanner.lvar_scanner.scanLVars
import type_map.TypeMap
import type.Type

class Compiler(val sources: Map<String, String>) {
  val compilationUnits = sources.map { (filePath, source) ->
    CompilationUnit(source, filePath.split("/"))
  }
  val lvarTypes = mutableMapOf<List<String>, Map<String, Type> >()
  val allFunctions = FunctionMap()
  val allTypes = TypeMap()

  init {
    compilationUnits.forEach { cu ->
      allTypes.update(cu.localTypes, cu.modulePath)
    }

    compilationUnits.forEach { cu ->
      allTypes.update_aliases(cu.aliases, cu.modulePath)
    }

    val builtInAliases = mapOf(
      TypeAST("Bool") to TypeAST("Bool", listOf("stdlib", "_core", "bool")),
      TypeAST("Word") to TypeAST("Word", listOf("stdlib", "_core", "word")),
      TypeAST("String") to TypeAST("String", listOf("stdlib", "_core", "string")),
      TypeAST("Nil") to TypeAST("Nil", listOf("stdlib", "_core", "nil")),
      TypeAST("Sys") to TypeAST("Sys", listOf("stdlib", "_core", "sys")),
      TypeAST("Lambda") to TypeAST("Lambda", listOf("stdlib", "_core", "lambda"))
    )
    allTypes.update_aliases(builtInAliases, listOf<String>())

    compilationUnits.forEach { cu ->
      val functionMap = scanFunctions(cu.topLevel, cu.modulePath, allTypes)
      allFunctions.update(functionMap)
    }

    compilationUnits.forEach { cu ->
      lvarTypes.putAll(scanLVars(cu.topLevel, cu.modulePath, allTypes, allFunctions)) // TODO check for duplicates?
    }
  }

  fun emit(dir: Path): List<String> {
    return compilationUnits.map { cu ->
      val outFilename = cu.modulePath.joinToString(".")
      val outFile = Files.newBufferedWriter(Paths.get(dir.toString(), outFilename))
      try {
        emitCompilationUnit(
          outFile,
          cu,
          cu.modulePath,
          lvarTypes,
          allFunctions,
          allTypes
        )
      }
      finally { outFile.close() }
      outFilename
    }
  }
}

class CompilerError(msg: String) : Exception(msg)

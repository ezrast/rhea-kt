package emitter

import java.io.BufferedWriter

import ast.*
import compilation_unit.CompilationUnit
import function_map.FunctionMap
import function_signature.FunctionSignature
import qualified_name.QualifiedName as QN
import scanner.constant_scanner.Constants
import type.Type
import type_map.TypeMap

fun emitCompilationUnit(
  outFile: BufferedWriter,
  cu: CompilationUnit,
  modulePath: List<String>,
  lvarTypes: Map<List<String>, Map<String, Type> >,
  functionMap: FunctionMap,
  allTypes: TypeMap
) {
  val emitter = Emitter(outFile, cu, modulePath, lvarTypes, functionMap, allTypes)
  emitter.emitTopLevel()
}

private class Emitter(
  val outFile: BufferedWriter,
  cu: CompilationUnit,
  val modulePath: List<String>,
  val lvarTypes: Map<List<String>, Map<String, Type> >,
  val functionMap: FunctionMap,
  val allTypes: TypeMap
) {
  val path = mutableListOf<String>()
  var indent = 0
  var newLine = true
  var lastExprId = 0
  fun getExprId() = lastExprId++
  val constants = cu.constants

  val topLevel = cu.topLevel
  val localTypes = cu.localTypes

  val definedFunctions = mutableSetOf<QN>()
  val usedFunctions = mutableSetOf<QN>()

  fun put(vararg things: String) {
    putIndent()
    things.forEach { outFile.write(it) }
  }

  fun put(thing: Nested) {
    put("${thing.ir} ${thing.id}")
  }

  fun putIndent() {
    if(newLine) {
      0.until(indent).forEach{ outFile.write("  ") }
      newLine = false
    }
  }

  fun putLine(vararg things: String) {
    put(*things)
    outFile.write("\n")
    newLine = true
  }

  fun putLine() {
    outFile.write("\n")
    newLine = true
  }

  fun pathStr(): String = (modulePath + path).joinToString(".")

  fun emitTopLevel() {
    allTypes.map.forEach { (qtn, type) ->
      put("%\"", qtn.toString(), "\" = type ", type.irRepr(), "\n")
    }
    putLine()

    constants.strings.forEach{ (stringLit, idx) ->
      put("@STR_${idx}.inner = private constant [${stringLit.value.size} x i8] c\"")
      put(stringLit.asString())
      putLine("\"")
      putLine("@STR_${idx} = private constant %.String { ${allTypes.ir(modulePath, "Word")} ${stringLit.value.size}, i8* getelementptr([${stringLit.value.size} x i8], [${stringLit.value.size} x i8]* @STR_${idx}.inner, i64 0, i64 0) }")
    }

    constants.lambdas.forEach{ (lambdaLit, idx) ->
      putLine("define void @LAMBDA_${idx}() {")
      indent++
      lambdaLit.body.forEach{ emitExpr(it) }
      indent--
      putLine("ret void")
      putLine("}")
    }
    putLine()

    if(null != topLevel.actors.find { it.name == "Main" }) {
      put("""
        |define i32 @_start() {
        |  call i32 @main()
        |  call ${allTypes.ir(modulePath, "Word")} @"stdlib._core.sys.Sys.exit"(${allTypes.ir(modulePath, "Word")} 0)
        |  unreachable
        |}
        |
        |define i32 @main() {
        |  call void @${modulePath.joinToString(".")}.Main.init()
        |  ret i32 0
        |}
        |
        |
      """.trimMargin())
      usedFunctions.add(QN(listOf("stdlib", "_core", "sys", "Sys"), "exit"))
    }

    topLevel.modules.forEach { emitModule(it) }
    topLevel.actors.forEach { emitActor(it) }

    (usedFunctions - definedFunctions).forEach { functionQN ->
      val signature = functionMap[functionQN]
      put("declare ", allTypes.ir(signature.retQN), " @\"", functionQN.toString(), "\"(")
      signature.argumentQNs.forEachIndexed { idx, typeQN ->
        if(idx > 0) { put(", ") }
        put(allTypes.ir(typeQN))
      }
      putLine(")")
    }
    putLine()

  }

  fun emitActor(node: ActorAST) {
    path.add(node.name)
    node.states.forEach{ emitState(it) }
    node.defs.forEach{ emitDef(it) }
    node.rawDefs.forEach{ emitRawDef(it) }
    path.pop()
  }

  fun emitModule(node: ModuleAST) {
    path.add(node.name)
    node.defs.forEach{ emitDef(it) }
    node.rawDefs.forEach{ emitRawDef(it) }
    node.actors.forEach { emitActor(it) }
    path.pop()
  }

  fun emitState(node: StateAST) {
    path.add(node.name)

    put("define void @${pathStr()}(")
    0.until(node.arguments.count()).forEach { idx ->
      if(idx != 0) { put(", ") }
      put("%$${idx}")
    }
    putLine(") {")
    indent++
    lvarTypes.getValue(modulePath + path).forEach{ _ -> // (name, type) ->
      throw Exception("NYI - ${node}")
      // putLine("%${name} = alloca ${allTypes.ir(modulePath, type)}")
    }
    // TODO arguments

    node.body.forEach{ emitExpr(it) }
    putLine("ret void")
    indent--
    putLine("}")
    putLine()

    path.pop()
  }

  fun emitDef(node: DefAST) {
    throw Exception("NYI - ${node}")
    // path.add(node.name)

    // put("define ${allTypes.ir(modulePath + path, node.type)} @${pathStr()}(")  //)
    // node.arguments.forEachIndexed{ idx, _ ->
    //   if(idx != 0) { put(", ") }
    //   put("%$${idx}")
    // }
    // putLine(") {")
    // indent++
    // lvarTypes[pathStr()]!!.forEach{ (name, type) ->
    //   putLine("%${name} = alloca ${allTypes.ir(modulePath, type)}")
    // }
    // // TODO arguments

    // node.body.forEach{ emitExpr(it) }
    // if(!node.body.any{ it is RetAST }) {
    //   putLine("ret void")
    // }
    // indent--
    // putLine("}")
    // putLine()

    // path.pop()
  }

  fun emitRawDef(node: RawDefAST) {
    definedFunctions.add(QN(modulePath + path, node.name))
    path.add(node.name)

    put("define ${allTypes.ir(modulePath + path, node.type)} @\"${pathStr()}\"(")  //)
    node.arguments.forEachIndexed{ idx, arg ->
      if(idx != 0) { put(", ") }
      emitType(arg.type)
      put(" %${arg.name}")
    }
    putLine(") {")
    indent++

    node.strings.forEach{ putLine(it.asString()) }

    indent--
    putLine("}")
    putLine()

    path.pop()
  }

  fun emitType(node: TypeAST) {
    put(allTypes.ir(modulePath + path, node))
  }

  fun emitExpr(node: ExpressionAST): Nested {
    when(node) {
      is CallAST -> {
        val nestedArgs = node.arguments.map{ emitExpr(it) }
        val exprId = (getExprId())

        val functionQN = functionMap.qualify(modulePath + path, node)
        val signature = functionMap[functionQN]
        usedFunctions.add(functionQN)
        val typeIR = allTypes.ir(signature.retQN)
        put("%.${exprId} = call ${typeIR} @\"")
        node.namespace.forEach{ put(it, ".") }
        put("${node.callee}\"(")

        nestedArgs.forEachIndexed { argIdx, arg ->
          if(argIdx > 0) { put(", ") }
          put(arg)
        }
        putLine(")")

        return Nested(typeIR, "%.${exprId}")
      }

      is StringLitAST -> {
        val exprId = getExprId()
        val idx = constants.strings[node]
        putLine("%.${exprId} = load %.String, %.String* @STR_${idx}")
        return Nested(allTypes.ir(modulePath, "String"), "%.${exprId}")
      }

      is MethodAST -> {
        throw Exception("NYI - ${node}")
      }

      // is AssignmentAST -> {
      //   val rhs = emitExpr(node.value)
      //   val type = lvarTypes.getValue(pathStr()).getValue(node.lvar.name)
      //   putLine("store ${rhs}, ${allTypes.ir(modulePath, type)}* %${node.lvar.name}")
      //   return rhs
      // }

      // is LVarAST -> {
      //   val exprId = getExprId()
      //   val type = lvarTypes.getValue(pathStr()).getValue(node.name)
      //   putLine("%.${exprId} = load ${allTypes.ir(modulePath, type)}, ${allTypes.ir(modulePath, type)}* %${node.name}")

      //   return Nested(type, "%.${exprId}")
      // }

      is IntLitAST -> {
        return Nested(allTypes.ir(modulePath, "Word"), node.value.toString())
      }

      is LambdaLitAST -> {
        throw Exception("NYI - ${node}")
      }

      // is RetAST -> {
      //   putLine("ret ${emitExpr(node.value)}")
      //   return Nested(type.noReturn, "Error: Don't nest return expressions")
      // }

      else -> throw Exception("NYI - ${node}")
    }
  }

  class Nested(val ir: String, val id: String) {}
}

fun <T> MutableList<T>.pop() = removeAt(lastIndex)

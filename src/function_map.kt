package function_map

import ast.CallAST
import function_signature.FunctionSignature
import qualified_name.QualifiedName as QN

// Basically just a map of function name -> signature, but with some methods
// for finding types within namespaces
class FunctionMap(val map: MutableMap<QN, FunctionSignature> = mutableMapOf<QN, FunctionSignature>()) {

  operator fun set(name: QN, value: FunctionSignature): FunctionSignature {
    if(map.containsKey(name)) {
      throw Exception("Redefinition of type ${name}")
    }
    map[name] = value
    return value
  }

  operator fun get(name: QN): FunctionSignature {
    val ret = map[name]
    if(ret == null) {
      throw Exception("No such type: ${name}")
    }
    return ret
  }

  operator fun get(implicitPath: List<String>, explicitPath: CallAST): FunctionSignature {
    var root = implicitPath
    while (true) {
      val result = map[QN(root + explicitPath.namespace, explicitPath.callee)]
      if (result != null) return result
      if (root.isEmpty()) throw Exception("No such type: ${explicitPath} in ${implicitPath}. Available types: ${map.keys}")
      root = root.dropLast(1)
    }
  }

  fun qualify(implicitPath: List<String>, explicitPath: CallAST): QN {
    var root = implicitPath
    while (true) {
      val qn = QN(root + explicitPath.namespace, explicitPath.callee)
      if (map.containsKey(qn)) return qn
      if (root.isEmpty()) throw Exception("No such type: ${explicitPath} in ${implicitPath}. Available types: ${map.keys}")
      root = root.dropLast(1)
    }
  }

  fun update(other: FunctionMap) {
    other.map.forEach { (key, value) -> this[key] = value }
  }

  // Like update(), but non-destructive
  fun merge(other: FunctionMap): FunctionMap {
    val ret = FunctionMap(map.toMutableMap())
    ret.update(other)
    return ret
  }

  fun forEach(lambda: (Map.Entry<QN, FunctionSignature>) -> Unit) = map.forEach(lambda)
}

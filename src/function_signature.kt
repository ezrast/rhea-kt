package function_signature

import qualified_name.QualifiedName as QN

data class FunctionSignature(
  val argumentQNs: List<QN>,
  val retQN: QN
) {}

package lexer

import token.Mode as Mode
import token.Transition as Transition
import token.Push as Push
import token.Pop as Pop
import token.getTokenTypes as getTokenTypes
import token.Token as Token
import token.TokenType as TokenType
import token.TokenTypeID as TokenTypeID

fun lex(source: String): List<Token> {
  val lines: List<String> = source.split("\n")
  var col = 0
  var row = 0
  var newLine = true
  val tokens = mutableListOf<Token>()
  var comment = StringBuilder()
  val modeStack = mutableListOf(Mode.Main)

  while(true) {
    // Make sure there is something to consume
    while(col >= lines[row].length) {
      newLine = true
      ++row
      if(row >= lines.size) return tokens
      col = 0
    }

    val remainingStr = lines[row].drop(col)
    var tokenSize = 0
    val tokenType = getTokenTypes(modeStack.last()).find find@{
      tokenSize = it.pattern.match(remainingStr)
      return@find tokenSize != 0
    }
    if (tokenType == null) {
      throw Exception("Lexer error at ${row+1}:${col+1}\n${lines[row].trimEnd()}\n${"".padStart(col)}^\n")
    }

    val str = remainingStr.take(tokenSize)
    when(val typeID = tokenType.typeID) {
      TokenTypeID.None -> {}
      TokenTypeID.Comment -> {
        if(newLine) {
          comment.append(str.drop(1))
          comment.append("\n")
        }
      }
      else -> {
        tokens.add(Token(typeID, str, row, col, newLine, comment.toString()))
        comment.clear()
        newLine = false
      }
    }
    col += tokenSize
    when(val transition = tokenType.transition) {
      is Push -> modeStack.add(transition.nextMode)
      is Pop -> modeStack.removeAt(modeStack.size - 1)
    }
  }
}

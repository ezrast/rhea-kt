package parser

import ast.*
import lexer.lex
import token.Token as Token
import token.TokenTypeID as TTID

fun parseTopLevel(source: String): ModuleAST =
  Parser(source).parseTopLevel()

private class Parser(val source: String) {
  val tokens = lexer.lex(source)
  var idx = 0

  fun peek(offset: Int = 0): TTID? =
    tokens.getOrNull(idx + offset)?.typeID

  fun shift(): Token? =
    tokens.getOrNull(idx++)

  fun newLine(offset: Int = 0): Boolean =
    tokens.getOrNull(idx + offset)?.newLine ?: false

  fun want(vararg typeIDs: TTID): Token? {
    if(peek() in typeIDs) return shift()
    return null
  }

  fun wantList(vararg typeIDs: TTID): List<Token> {
    val ret = mutableListOf<Token>()
    loop@ while(true) {
      when(val token = want(*typeIDs)) {
        null -> break@loop
        else -> { ret.add(token) }
      }
    }
    return ret
  }

  fun need(vararg typeIDs: TTID): Token {
    val tok = shift()
    if(tok != null && tok.typeID in typeIDs) return tok
    error("Expected ${typeIDs.joinToString(",")}; got ${tok?.typeID ?: "EOF"}")
  }

  fun expected(str: String): Nothing {
    val tok = shift()
    error("Expected ${str}; got ${tok?.typeID ?: "EOF"}")
  }

  fun error(msg: String): Nothing =
    throw ParseError(msg, shift())

////////// ASTNode parsing methods

  fun parseTopLevel(): ModuleAST = parseModuleInner("_top", true)

  fun parseModule(): ModuleAST {
    need(TTID.KwModule)
    val module = parseModuleInner(need(TTID.Constant).str)
    need(TTID.KwEnd)
    return module
  }

  fun parseModuleInner(name: String, topLevel: Boolean = false): ModuleAST {
    val aliases = mutableListOf<AliasAST>()
    val actors = mutableListOf<ActorAST>()
    val modules = mutableListOf<ModuleAST>()
    val slices = mutableListOf<SliceAST>()
    val defs = mutableListOf<DefAST>()
    val rawDefs = mutableListOf<RawDefAST>()
    val biTypes = mutableListOf<BiValueAST>()

    val sentinel = if(topLevel) null else TTID.KwEnd
    val sentinelStr = if(topLevel) "EOF" else "End"

    loop@ while(true) {
      when(peek()) {
        TTID.KwAlias -> aliases.add(parseAlias())
        TTID.KwActor -> actors.add(parseActor())
        TTID.KwSlice -> slices.add(parseSlice())
        TTID.KwModule -> modules.add(parseModule())
        TTID.KwDef -> defs.add(parseDef())
        TTID.KwRawDef -> rawDefs.add(parseRawDef())
        TTID.KwBiValue -> biTypes.add(parseBiValue())
        sentinel -> break@loop
        else -> expected("Actor, Module, Def, Slice, or ${sentinelStr}")
      }
    }

    return ModuleAST(name, aliases, actors, modules, slices, defs, rawDefs, biTypes)
  }

  fun parseAlias(): AliasAST {
    need(TTID.KwAlias)
    val type = parseType()
    val alias = if(want(TTID.Rocket) != null) parseType() else TypeAST(type.name)
    return AliasAST(type, alias)
  }

  fun parseBiValue(): BiValueAST {
    need(TTID.KwBiValue)
    val id = need(TTID.Constant)
    need(TTID.KwIR)
    need(TTID.QuoteL)
    val irName = parseStrBody()
    need(TTID.QuoteR)
    val rawDefs = mutableListOf<RawDefAST>()

    while(want(TTID.KwEnd) == null) {
      rawDefs.add(parseRawDef())
    }

    return BiValueAST(id.str, irName, rawDefs)
  }

  fun parseActor(): ActorAST {
    need(TTID.KwActor)
    val id = need(TTID.Constant)
    val states = mutableListOf<StateAST>()
    val defs = mutableListOf<DefAST>()
    val rawDefs = mutableListOf<RawDefAST>()

    while(want(TTID.KwEnd) == null) {
      when(peek()) {
        TTID.KwState -> states.add(parseState())
        TTID.KwDef -> defs.add(parseDef())
        TTID.KwRawDef -> rawDefs.add(parseRawDef())
        else -> expected("State or Def")
      }
    }

    return ActorAST(id.str, states, defs, rawDefs)
  }

  fun parseSlice(): SliceAST {
    need(TTID.KwSlice)
    val id = need(TTID.Constant)
    val inits = mutableListOf<InitAST>()
    val defs = mutableListOf<DefAST>()
    var fin: List<ExpressionAST>? = null

    while(want(TTID.KwEnd) == null) {
      when(peek()) {
        TTID.KwInit -> inits.add(parseInit())
        TTID.KwDef -> defs.add(parseDef())
        TTID.KwFin -> {
          if(fin != null) expected("Only one Fin")
          need(TTID.KwFin)
          fin = parseBlock(false, false).component3()
        }
        else -> expected("Init, Def or Fin")
      }
    }

    return SliceAST(id.str, inits, defs, fin ?: listOf<ExpressionAST>())
  }

  fun parseState(): StateAST {
    need(TTID.KwState)
    val id = need(TTID.Identifier)
    val (arguments, _, body) = parseBlock(true, false)

    return StateAST(id.str, arguments, body)
  }

  fun parseInit(): InitAST {
    need(TTID.KwInit)
    val id = need(TTID.Identifier)
    val (arguments, _, body) = parseBlock(true, false)

    return InitAST(id.str, arguments, body)
  }

  fun parseDef(): DefAST {
    need(TTID.KwDef)
    val id = need(TTID.Identifier)
    val (arguments, retType, body) = parseBlock(true, true)

    return DefAST(id.str, arguments, retType, body)
  }

  fun parseRawDef(): RawDefAST {
    need(TTID.KwRawDef)
    val id = need(TTID.Identifier, TTID.Plus, TTID.Minus, TTID.Compare, TTID.Slash, TTID.Star)
    need(TTID.BraceL)

    val arguments = mutableListOf<ArgumentAST>()
    if(want(TTID.Pipe) != null) {
      arguments.add(parseArgument())
      while(want(TTID.Comma) != null) {
        arguments.add(parseArgument())
      }
    }

    val retType = if(want(TTID.Arrow) != null)
      parseType()
    else
      TypeAST("Nil", listOf("stdlib", "_core", "nil"))

    val strings = mutableListOf<StringLitAST>()
    while(true) {
      strings.add(parseQuote())
      if(want(TTID.Comma) == null) break
    }
    need(TTID.BraceR)

    return RawDefAST(id.str, arguments, retType, strings)
  }

  fun parseBlock(doArguments: Boolean, doRetType: Boolean): Triple<List<ArgumentAST>, TypeAST, List<ExpressionAST> > {
    need(TTID.BraceL)

    val body = mutableListOf<ExpressionAST>()
    val arguments = mutableListOf<ArgumentAST>()
    var needsSemi = false

    if(doArguments && want(TTID.Pipe) != null) {
      arguments.add(parseArgument())
      while(want(TTID.Comma) != null) {
        arguments.add(parseArgument())
      }
      needsSemi = true
    }

    val retType = if(doRetType && want(TTID.Arrow) != null) {
      needsSemi = true
      parseType()
    } else {
      TypeAST("Nil", listOf("stdlib", "_core", "nil"))
    }

    loop@ while(true) {
      when {
        want(TTID.BraceR) != null -> {
          break@loop
        }
        want(TTID.Semicolon) != null -> {
          needsSemi = false
        }
        else -> {
          if(needsSemi && !newLine()) expected("Expressions must be separated by ';' or '\n'")
          body.add(parseExpression())
          needsSemi = true
        }
      }
    }

    return Triple(arguments, retType, body)
  }

  fun parseArgument(): ArgumentAST {
    val type = parseType()
    val name = need(TTID.Identifier)

    return ArgumentAST(name.str, type)
  }

  fun parseType(): TypeAST {
    val path = wantList(TTID.Module).map{ module -> module.str.dropLast(1) }
    val name = need(TTID.Constant)
    return TypeAST(name.str, path)
  }

  fun parseExpression(): ExpressionAST {
    val leftmost = parseExpressionNoBinop()

    if(want(TTID.Assign) != null) {
      if(!(leftmost is LVarAST)) {
        expected("Left-hand side of assignment operator must be a local variable")
      }
      val other = parseExpression()
      return AssignmentAST(leftmost, other)
    }

    if(want(TTID.Dot) != null) {
      val callee = need(TTID.Identifier).str

      val arguments = mutableListOf<ExpressionAST>()
      if(!newLine()) {
        if(want(TTID.ParenL) != null) { // Args are in parens
          while(true) {
            arguments.add(parseExpression())
            if(want(TTID.ParenR) != null) break
            need(TTID.Comma)
          }
        }
        else if(peek() in EXPR_BEGINNERS) { // Args are hangin' loose
          arguments.add(parseExpression())
          while(want(TTID.Comma) != null) {
            arguments.add(parseExpression())
          }
        }
      }

      return MethodAST(leftmost, callee, arguments)
    }

    // TODO make sure this precedence makes sense
    val binaryPrecedence = listOf(
      listOf( TTID.Star, TTID.Slash ),
      listOf( TTID.Plus, TTID.Minus ),
      listOf( TTID.Compare, TTID.Less, TTID.Greater, TTID.LessEq, TTID.GreaterEq ),
      listOf( TTID.And ),
      listOf( TTID.Or )
    )

    val opChain = mutableListOf<Token>()
    val exprs = mutableListOf(leftmost)

    while(peek() in binaryPrecedence.flatten()) {
      opChain.add(shift()!!)
      exprs.add(parseExpressionNoBinop())
    }

    binaryPrecedence.forEach { ops ->
      var idx = opChain.indexOfFirst{ it.typeID in ops }
      while(idx != -1) {
        val op = opChain.removeAt(idx)
        val lhs = exprs.removeAt(idx)
        val rhs = exprs[idx]
        exprs[idx] = MethodAST(lhs, op.str, listOf(rhs))
        idx = opChain.indexOfFirst{ it.typeID in ops }
      }
    }

    if(opChain.isNotEmpty()) throw(Exception("Bug! Binary ops: ${opChain}"))
    if(exprs.size != 1) throw(Exception("Bug! Expressions: ${exprs}"))

    return exprs[0]
  }

  val EXPR_BEGINNERS = listOf(TTID.Constant, TTID.Identifier, TTID.Int,
    TTID.QuoteL, TTID.ParenL, TTID.BraceL, TTID.KwRet)
  fun parseExpressionNoBinop(): ExpressionAST {
    when(peek()) {
      TTID.Module, TTID.Constant -> return parseCall()
      TTID.Identifier -> {
        // If this is followed by an expression, it's a function call
        // Arg-less calls require parens to disambiguate from lvars.
        if(peek(1) in EXPR_BEGINNERS && !newLine(1))
          return parseCall()
        else
          return parseLvar()
      }
      TTID.Int -> return parseInt()
      TTID.QuoteL -> return parseQuote()
      TTID.ParenL -> {
        shift()
        val expr = parseExpression()
        need(TTID.ParenR)
        return expr
      }
      TTID.BraceL -> return parseLambda()
      TTID.KwRet -> return parseRet()
      else -> expected("Expression")
    }
  }

  fun parseCall(): CallAST {
    val namespace = wantList(TTID.Module).map{ module -> module.str.dropLast(1) }.toMutableList()
    var name = want(TTID.Constant)
    while(name != null) {
      namespace.add(name.str)
      need(TTID.Dot)
      name = want(TTID.Constant)
    }
    val id = need(TTID.Identifier)

    val arguments = mutableListOf<ExpressionAST>()
    if(!newLine()) {
      if(want(TTID.ParenL) != null) { // Args are in parens
        while(true) {
          arguments.add(parseExpression())
          if(want(TTID.ParenR) != null) break
          need(TTID.Comma)
        }
      }
      else if(peek() in EXPR_BEGINNERS) { // Args are hangin' loose
        arguments.add(parseExpression())
        while(want(TTID.Comma) != null) {
          arguments.add(parseExpression())
        }
      }
    }

    return CallAST(id.str, namespace, arguments)
  }

  fun parseLvar(): LVarAST {
    val id = need(TTID.Identifier)
    return LVarAST(id.str)
  }

  fun parseQuote(): StringLitAST {
    need(TTID.QuoteL)
    val value = parseStrBody().toByteArray()
    need(TTID.QuoteR)

    return StringLitAST(value)
  }

  fun parseStrBody(): String {
    return buildString {
      loop@ while(true) {
        when(peek()) {
          TTID.StringBody -> append(need(TTID.StringBody).str)
          TTID.EscapeByte -> {
            when(val escape = need(TTID.EscapeByte).str) {
              "\\n" -> append('\n')
              "\\\"" -> append('"')  //"
              else -> error("Unrecognized escape: ${escape}")
            }
          }
          TTID.EscapeDecimal -> append(need(TTID.EscapeDecimal).str.drop(1).toByteOrNull()!!.toChar()) // TODO check instead of !!
          TTID.EscapeHex -> append(need(TTID.EscapeHex).str.drop(2).toByteOrNull(16)!!.toChar()) // TODO check instead of !!
          else -> break@loop
        }
      }
    }
  }

  fun parseInt(): IntLitAST {
    val int = need(TTID.Int)
    val value = int.str.toIntOrNull()!! // TODO check instead of !!
    return IntLitAST(value)
  }

  fun parseLambda(): LambdaLitAST {
    val (arguments, retType, body) = parseBlock(true, true)

    return LambdaLitAST(arguments, retType, body)
  }

  fun parseRet(): RetAST {
    need(TTID.KwRet)
    return RetAST(parseExpression())
  }



}

class ParseError(
  val msg: String,
  val token: Token?
) : Exception(
  when(token) {
    is Token -> "${msg} at ${token.str} (${token.row + 1}:${token.col + 1})"
    else -> "${msg} at EOF"
  }
)

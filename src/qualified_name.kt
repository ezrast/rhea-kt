package qualified_name

data class QualifiedName(
  val path: List<String>,
  val name: String
) {
  fun prefix(prefix: List<String>?) = if (prefix == null) this else QualifiedName(prefix + path, name)

  override fun toString() = path.joinToString(".") + "." + name
}

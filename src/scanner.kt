package scanner

import ast.*

abstract class Scanner {
  val path = mutableListOf<String>()

  open fun push(part: String) {
    path.add(part)
  }

  open fun pop() {
    path.removeAt(path.lastIndex)
  }

  open fun scanBiType(node: BiValueAST) {
    node.rawDefs.forEach{ push(it.name); scanRawDef(it); pop() }
  }

  open fun scanActor(node: ActorAST) {
    node.states.forEach{ push(it.name); scanState(it); pop() }
    node.defs.forEach{ push(it.name); scanDef(it); pop() }
    node.rawDefs.forEach{ push(it.name); scanRawDef(it); pop() }
  }

  open fun scanSlice(node: SliceAST) {
    node.inits.forEach{ push(it.name); scanInit(it); pop() }
    node.defs.forEach{ push(it.name); scanDef(it); pop() }
    push("Fin")
    node.fin.forEach{ scanExpr(it) }
    pop()
  }

  open fun scanModule(node: ModuleAST) {
    node.aliases.forEach{ scanAlias(it) }
    node.actors.forEach{ push(it.name); scanActor(it); pop() }
    node.slices.forEach{ push(it.name); scanSlice(it); pop() }
    node.modules.forEach{ push(it.name); scanModule(it); pop() }
    node.rawDefs.forEach{ push(it.name); scanRawDef(it); pop() }
    node.defs.forEach{ push(it.name); scanDef(it); pop() }
    node.biTypes.forEach{ push(it.name); scanBiType(it); pop() }
  }

  open fun scanAlias(node: AliasAST) {
  }

  open fun scanState(node: StateAST) {
    node.body.forEach{ scanExpr(it) }
  }

  open fun scanInit(node: InitAST) {
    node.body.forEach{ scanExpr(it) }
  }

  open fun scanDef(node: DefAST) {
    node.body.forEach{ scanExpr(it) }
  }

  open fun scanRawDef(node: RawDefAST) {
  }

  open fun scanArgument(node: ArgumentAST) {
  }

  open fun scanType(node: TypeAST) {
  }

  open fun scanExpr(node: ExpressionAST) {
    when(node) {
      is CallAST -> scanCall(node)
      is MethodAST -> scanMethod(node)
      is AssignmentAST -> scanAssignment(node)
      is LVarAST -> scanLVar(node)
      is LambdaLitAST -> scanLambdaLit(node)
      is StringLitAST -> scanStringLit(node)
      is IntLitAST -> scanIntLit(node)
      is RetAST -> scanRet(node)
    }.apply{} // force exhaustive
  }

  open fun scanCall(node: CallAST) {
    node.arguments.forEach { scanExpr(it) }
  }

  open fun scanMethod(node: MethodAST) {
    scanExpr(node.receiver)
    node.arguments.forEach { scanExpr(it) }
  }

  open fun scanAssignment(node: AssignmentAST) {
    scanExpr(node.lvar)
    scanExpr(node.value)
  }

  open fun scanLVar(node: LVarAST) {
  }

  open fun scanLambdaLit(node: LambdaLitAST) {
    node.body.forEach{ scanExpr(it) }
  }

  open fun scanStringLit(node: StringLitAST) {
  }

  open fun scanIntLit(node: IntLitAST) {
  }

  open fun scanRet(node: RetAST) {
    scanExpr(node.value)
  }
}

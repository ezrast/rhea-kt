package scanner.alias_scanner

import ast.*
import scanner.Scanner

fun scanAliases(ast: ModuleAST): Map<TypeAST, TypeAST> {
  val als = AliasScanner()
  als.scanModule(ast)
  return als.map
}

private class AliasScanner : Scanner() {
  val map = mutableMapOf<TypeAST, TypeAST>()

  override fun scanAlias(node: AliasAST) {
    map[node.alias] = node.type
  }
}

package scanner.constant_scanner

import ast.*
import scanner.Scanner

fun scanConstants(ast: ModuleAST): Constants {
  val scanner = ConstantScanner()
  scanner.scanModule(ast)
  return scanner.constants
}

class Constants {
  val strings = mutableMapOf<StringLitAST, Int>()
  val lambdas = mutableMapOf<LambdaLitAST, Int>()
}

class ConstantScanner : Scanner() {
  val constants = Constants()

  override fun scanStringLit(node: StringLitAST) {
    if(constants.strings[node] == null) {
      constants.strings[node]= constants.strings.size
    }
  }

  override fun scanLambdaLit(node: LambdaLitAST) {
    if(constants.lambdas[node] == null) {
      constants.lambdas[node] = constants.lambdas.size
    }
    super.scanExpr(node)
  }
}

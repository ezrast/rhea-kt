package scanner.function_scanner

import ast.*
import function_map.FunctionMap
import function_signature.FunctionSignature
import qualified_name.QualifiedName as QN
import scanner.Scanner
import type.BiValue
import type_map.TypeMap

fun scanFunctions(ast: ModuleAST, modulePath: List<String>, typeMap: TypeMap): FunctionMap {
  val fs = FunctionScanner(modulePath, typeMap)
  fs.scanModule(ast)
  return fs.functionMap
}

private class FunctionScanner(
  val modulePath: List<String>,
  val typeMap: TypeMap
) : Scanner() {
  val functionMap = FunctionMap()

  override fun scanInit(node : InitAST) {
    val name = QN(modulePath + path.dropLast(1), path.last())
    val argumentQNs = node.arguments.map{ typeMap.qualify(modulePath, it.type) }
    val retQN = QN(listOf("stdlib", "_core", "nil"), "Nil")
    functionMap[name] = FunctionSignature(argumentQNs, retQN)
    super.scanInit(node)
  }

  override fun scanDef(node : DefAST) {
    val name = QN(modulePath + path.dropLast(1), path.last())
    val argumentQNs = node.arguments.map{ typeMap.qualify(modulePath, it.type) }
    val retQN = typeMap.qualify(modulePath, node.type)
    functionMap[name] = FunctionSignature(argumentQNs, retQN)
    super.scanDef(node)
  }

  override fun scanRawDef(node : RawDefAST) {
    val name = QN(modulePath + path.dropLast(1), path.last())
    val argumentQNs = node.arguments.map{ typeMap.qualify(modulePath, it.type) }
    val retQN = typeMap.qualify(modulePath, node.type)
    functionMap[name] = FunctionSignature(argumentQNs, retQN)
    super.scanRawDef(node)
  }
}

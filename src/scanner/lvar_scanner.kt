package scanner.lvar_scanner

import ast.*
import compilation_unit.CompilationUnit
import function_map.FunctionMap
import function_signature.FunctionSignature
import qualified_name.QualifiedName as QN
import scanner.Scanner
import type.Type
import type_map.TypeMap

fun scanLVars(
  ast: ModuleAST,
  modulePath: List<String>,
  allTypes: TypeMap,
  functionMap: FunctionMap
): Map<List<String>, Map<String, Type> > {
  val lvs = LVarScanner(modulePath, allTypes, functionMap)
  lvs.scanModule(ast)
  return lvs.lvarTypes
}
// Maps function paths to lvar types
private class LVarScanner(
  val modulePath: List<String>,
  val allTypes: TypeMap,
  val functionMap: FunctionMap
) : Scanner() {
  val lvarTypes = mutableMapOf<List<String>, MutableMap<String, Type> >()

  fun resolve(type: TypeAST): Type {
    return allTypes[modulePath + path, type]
  }

  override fun scanInit(node: InitAST) {
    val types = lvarTypes.getOrPut(modulePath + path) { mutableMapOf<String, Type>() }

    node.arguments.forEach { arg ->
      if(types.contains(arg.name)) {
        throw Exception("Duplicate lvar def: ${arg.name}")
      }
      types[arg.name] = resolve(arg.type)
    }
    super.scanInit(node)
  }

  override fun scanState(node: StateAST) {
    val types = lvarTypes.getOrPut(modulePath + path) { mutableMapOf<String, Type>() }

    node.arguments.forEach { arg ->
      if(types.contains(arg.name)) {
        throw Exception("Duplicate lvar def: ${arg.name}")
      }
      types[arg.name] = resolve(arg.type)
    }
    super.scanState(node)
  }

  override fun scanDef(node: DefAST) {
    val types = lvarTypes.getOrPut(modulePath + path) { mutableMapOf<String, Type>() }

    node.arguments.forEach { arg ->
      if(types.contains(arg.name)) {
        throw Exception("Duplicate lvar def: ${arg.name}")
      }
      types[arg.name] = resolve(arg.type)
    }
    super.scanDef(node)
  }

  override fun scanRawDef(node: RawDefAST) {
    val types = lvarTypes.getOrPut(modulePath + path) { mutableMapOf<String, Type>() }

    node.arguments.forEach { arg ->
      if(types.contains(arg.name)) {
        throw Exception("Duplicate lvar def: ${arg.name}")
      }
      types[arg.name] = resolve(arg.type)
    }
    super.scanRawDef(node)
  }

  override fun scanAssignment(node: AssignmentAST) {
    super.scanAssignment(node)
    val types = lvarTypes.getOrPut(modulePath + path) { mutableMapOf<String, Type>() }
    if (types.contains(node.lvar.name)) {
      throw Exception("Duplicate lvar def: ${node.lvar.name}")
    }
    types[node.lvar.name] = findType(node.value)
  }

  override fun scanCall(node: CallAST) {
    val signature = functionMap[modulePath, node]
    val argumentTypes = node.arguments.map{ arg -> findType(arg) }
    val expectedTypes = signature.argumentQNs.map{ allTypes[it] }

    if(argumentTypes != expectedTypes) {
      throw WrongType("Function ${node.toString()} expected ${expectedTypes}, received ${argumentTypes}")
    }

    super.scanCall(node)
  }

  /////////

  fun findType(node: ExpressionAST): Type {
    return when(node) {
      is CallAST -> {
        allTypes[functionMap[modulePath + path, node].retQN]
      }

      is MethodAST -> {
        throw Exception("NYI - ${node}")
        // val receiverType = findType(node.receiver)
        // resolve(functionMap["${receiverType.friendly()}.${node.callee}"]!!.retType)
      }

      is AssignmentAST -> {
        findType(node.value)
      }

      is LVarAST -> {
        lvarTypes.getValue(modulePath + path).getValue(node.name)
      }

      is StringLitAST -> {
        allTypes[QN(listOf("stdlib", "_core", "string"), "String")]
      }

      is IntLitAST -> {
        allTypes[QN(listOf("stdlib", "_core", "word"), "Word")]
      }

      is LambdaLitAST -> {
        throw Exception("NYI - ${node}")
      }

      is RetAST -> {
        throw Exception("NYI - ${node}")
      }
    }
  }

  class UndefinedFunction(msg: String): Exception(msg)
  class WrongType(msg: String): Exception(msg)
}

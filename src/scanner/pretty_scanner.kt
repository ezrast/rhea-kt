package scanner.pretty_scanner

import ast.*
import scanner.Scanner

fun scanPretty(ast: ModuleAST): String {
  val ps = PrettyScanner()
  ps.scanModule(ast)
  return ps.builder.toString()
}

private class PrettyScanner : Scanner() {
  var indent = 0
  val builder = StringBuilder()

  override fun push(part: String) {
    super.push(part)
    0.until(indent).forEach{ builder.append("  ") }
    builder.append(part)
    builder.append("\n")
    indent++
  }

  override fun pop() {
    super.pop()
    indent--
  }
}

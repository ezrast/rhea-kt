package scanner.type_scanner

import ast.*
import qualified_name.QualifiedName as QN
import scanner.Scanner
import type.BiValue
import type.Actor
import type_map.TypeMap

fun scanTypes(ast: ModuleAST): TypeMap {
  val ts = TypeScanner()
  ts.scanModule(ast)
  return ts.typeMap
}

private class TypeScanner : Scanner() {
  val typeMap = TypeMap()

  override fun scanBiType(node: BiValueAST) {
    typeMap[QN(path.dropLast(1), path.last())] = BiValue(node.irName, node.name)
    super.scanBiType(node)
  }

  override fun scanActor(node: ActorAST) {
    typeMap[QN(path.dropLast(1), path.last())] = Actor(path.joinToString("."))
  }

}

package token

// Regexes seem buggy in Kotlin-native so here's my own half-baked
// expression matching scheme
interface Pattern {
  abstract fun match(matchee: String): Int

  operator fun plus(other: Pattern): Pattern = Both(this, other)
  operator fun div(other: Pattern): Pattern = HeadTail(this, other)
}

// Matches the pattern string exactly
private class Exactly(val pattern: String) : Pattern {
  override fun match(matchee: String): Int {
    if(matchee.startsWith(pattern)) return pattern.length
    return 0
  }
}

// Matches exactly one character in the pattern string
private class OneOf(val pattern: String) : Pattern {
  override fun match(matchee: String): Int {
    if(matchee[0] in pattern) return 1
    return 0
  }
}

// Matches one or more characters, all of which must be in the pattern string
private class SeveralOf(val pattern: String) : Pattern {
  override fun match(matchee: String): Int {
    var idx = 0
    while(matchee[idx] in pattern) {
      ++idx
      if(idx >= matchee.length) break
    }
    return idx
  }
}

// Matches one or more characters, none of which may be in the pattern string
private class AnythingBut(val pattern: String) : Pattern {
  override fun match(matchee: String): Int {
    var idx = 0
    while(!(matchee[idx] in pattern)) {
      ++idx
      if(idx >= matchee.length) break
    }
    return idx
  }
}

// Matches two patterns in succession (shorthand: +)
private class Both(val left: Pattern, val right: Pattern) : Pattern {
  override fun match(matchee: String): Int {
    var countLeft = left.match(matchee)
    if(countLeft == 0 || countLeft >= matchee.length) return 0
    var countRight = right.match(matchee.drop(countLeft))
    if(countRight == 0) return 0
    return countLeft + countRight
  }
}

// Matches the head exactly once and the tail one or zero times (shorthand: /)
private class HeadTail(val head: Pattern, val tail: Pattern) : Pattern {
  override fun match(matchee: String): Int {
    var idx = head.match(matchee)
    if(idx == 0) return 0
    if(matchee.length > idx) {
      idx += tail.match(matchee.drop(idx))
    }
    return idx
  }
}

// Matches to the end of the line
private class Anything(val maxLength: Int? = null) : Pattern {
  override fun match(matchee: String): Int {
    if(maxLength != null && matchee.length > maxLength ) return maxLength
    return matchee.length
  }
}

private val upperAlpha = OneOf("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
private val lowerAlpha = OneOf("abcdefghijklmnopqrstuvwxyz_")
private val identChars = SeveralOf("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_")
private val number = OneOf("0123456789") / SeveralOf("0123456789_")
private val hexDigit = OneOf("abcdefABCDEF0123456789")

data class Token(
  val typeID: TokenTypeID,
  val str: String,
  val row: Int,
  val col: Int,
  val newLine: Boolean,
  val comment: String?
) {}

data class TokenType(
  val pattern: Pattern,
  val typeID: TokenTypeID,
  val transition: Transition? = null
) {}

enum class Mode {
  Main,
  StringLiteral,
  MainBrace,
  MainParen,
  MainBracket,
}

interface Transition

data class Push(val nextMode: Mode) : Transition
object Pop : Transition

private val MainMode = listOf(
  TokenType(number + Exactly(".") + number / (Exactly("e") + number), TokenTypeID.Float),
  TokenType(number, TokenTypeID.Int),

  TokenType(Exactly("&&"), TokenTypeID.And),
  TokenType(Exactly("||"), TokenTypeID.Or),
  TokenType(Exactly(">="), TokenTypeID.GreaterEq),
  TokenType(Exactly("<="), TokenTypeID.LessEq),
  TokenType(Exactly(";"), TokenTypeID.Semicolon),
  TokenType(Exactly(":="), TokenTypeID.Assign),
  TokenType(Exactly(":"), TokenTypeID.Colon),
  TokenType(Exactly("->"), TokenTypeID.Arrow),
  TokenType(Exactly("!"), TokenTypeID.Bang),
  TokenType(Exactly("|"), TokenTypeID.Pipe),
  TokenType(Exactly("?"), TokenTypeID.Question),
  TokenType(Exactly(","), TokenTypeID.Comma),
  TokenType(Exactly("."), TokenTypeID.Dot),
  TokenType(Exactly("*"), TokenTypeID.Star),
  TokenType(Exactly("+"), TokenTypeID.Plus),
  TokenType(Exactly("-"), TokenTypeID.Minus),
  TokenType(Exactly("/"), TokenTypeID.Slash),
  TokenType(Exactly(">"), TokenTypeID.Greater),
  TokenType(Exactly("<"), TokenTypeID.Less),
  TokenType(Exactly("=="), TokenTypeID.Compare),
  TokenType(Exactly("=>"), TokenTypeID.Rocket),

  TokenType(Exactly("\""), TokenTypeID.QuoteL, Push(Mode.StringLiteral)),
  TokenType(Exactly("{"), TokenTypeID.BraceL, Push(Mode.MainBrace)),
  TokenType(Exactly("("), TokenTypeID.ParenL, Push(Mode.MainParen)),
  TokenType(Exactly("@["), TokenTypeID.SliceL, Push(Mode.MainBracket)),
  TokenType(Exactly("["), TokenTypeID.BracketL, Push(Mode.MainBracket)),

  TokenType(Exactly("Alias"), TokenTypeID.KwAlias),
  TokenType(Exactly("Module"), TokenTypeID.KwModule),
  TokenType(Exactly("BIValue"), TokenTypeID.KwBiValue),
  TokenType(Exactly("IR"), TokenTypeID.KwIR),
  TokenType(Exactly("Actor"), TokenTypeID.KwActor),
  TokenType(Exactly("State"), TokenTypeID.KwState),
  TokenType(Exactly("Slice"), TokenTypeID.KwSlice),
  TokenType(Exactly("Derive"), TokenTypeID.KwDerive),
  TokenType(Exactly("Init"), TokenTypeID.KwInit),
  TokenType(Exactly("Def"), TokenTypeID.KwDef),
  TokenType(Exactly("RawDef"), TokenTypeID.KwRawDef),
  TokenType(Exactly("Fin"), TokenTypeID.KwFin),
  TokenType(Exactly("Ret"), TokenTypeID.KwRet),
  TokenType(Exactly("End"), TokenTypeID.KwEnd),

  TokenType(Exactly("@") + lowerAlpha / identChars, TokenTypeID.Ivar),
  TokenType((lowerAlpha / identChars) + Exactly(":"), TokenTypeID.Module),
  TokenType(lowerAlpha / identChars, TokenTypeID.Identifier),
  TokenType(upperAlpha / identChars, TokenTypeID.Constant),

  TokenType(Exactly("#") + Anything(), TokenTypeID.Comment),
  TokenType(SeveralOf(" \t"), TokenTypeID.None)
)

fun getTokenTypes(mode: Mode): List<TokenType> = when(mode) {
  Mode.Main -> MainMode

  Mode.MainBrace -> listOf(
    TokenType(Exactly("}"), TokenTypeID.BraceR, Pop)
  ) + MainMode

  Mode.MainParen -> listOf(
    TokenType(Exactly(")"), TokenTypeID.ParenR, Pop)
  ) + MainMode

  Mode.MainBracket -> listOf(
    TokenType(Exactly("]"), TokenTypeID.BracketR, Pop)
  ) + MainMode

  Mode.StringLiteral -> listOf(
    TokenType(Exactly("\""), TokenTypeID.QuoteR, Pop),
    TokenType(Exactly("\\") + number, TokenTypeID.EscapeDecimal),
    TokenType(Exactly("\\x") + hexDigit + hexDigit, TokenTypeID.EscapeHex),
    TokenType(Exactly("\\{"), TokenTypeID.InterpL, Push(Mode.MainBrace)),
    TokenType(AnythingBut("\\\""), TokenTypeID.StringBody),
    // Only some of these are actually valid (\n, etc) but we take care of that in parsing
    TokenType(Exactly("\\") + Anything(1), TokenTypeID.EscapeByte)
  )
}

enum class TokenTypeID {
  Float, Int,
  And, Or,
  GreaterEq, LessEq,
  Semicolon,
  Colon, Arrow, Bang,
  Assign, Pipe, Question,
  Comma, Dot,
  Star, Plus, Minus, Slash,
  Greater, Less, Compare,
  Rocket,

  QuoteL, BraceL, ParenL, SliceL, BracketL,
  QuoteR, BraceR, ParenR, SliceR, BracketR,

  EscapeDecimal, EscapeHex, EscapeByte,
  InterpL, StringBody,

  KwAlias,
  KwModule,
  KwBiValue, KwIR,
  KwActor, KwState, KwSlice,
  KwDerive, KwInit, KwDef, KwRawDef, KwFin,
  KwRet,
  KwEnd,
  Ivar,

  Module,
  Identifier,
  Constant,

  Comment,
  None
}

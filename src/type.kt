package type

abstract class Type {
  abstract fun irRepr() : String
  abstract fun name() : String
  abstract fun friendly() : String
  abstract fun size() : Int
}

abstract class ValueType : Type()
abstract class ReferenceType : Type() {
  override fun size() = 64 // Should be platform-dependent
}

class Actor(val pathString: String): ReferenceType() {
  override fun irRepr() = "i8*"
  override fun name() = "%${pathString}"
  override fun friendly() = pathString
}

// built-ins
class BiValue(val irRepr: String, val friendly: String) : ValueType() {
  override fun irRepr() = irRepr
  override fun name() = "%${friendly()}"
  override fun friendly() = friendly
  override fun size() = throw Exception("BiValue.size() NYI")
}

object noReturn : ValueType() {
  override fun irRepr(): String { throw Exception("BINoReturn#ir_repr should not get called") }
  override fun name() = "void"
  override fun friendly() = ".NoReturn"
  override fun size() = 0
}

package type_map

import ast.TypeAST
import qualified_name.QualifiedName as QN
import type.Type

// Basically just a map of type name -> type, but with some methods
// for finding types within namespaces.
// The definitive collection of types available to a Compiler.
class TypeMap(val map: MutableMap<QN, Type> = mutableMapOf<QN, Type>()) {

  operator fun set(name: QN, value: Type): Type {
    if(map.containsKey(name)) {
      throw Exception("Redefinition of type ${name}")
    }
    map[name] = value
    return value
  }

  operator fun get(name: QN): Type {
    val ret = map[name]
    if(ret == null) {
      throw Exception("No such type: ${name}")
    }
    return ret
  }

  operator fun get(implicitPath: List<String>, explicitPath: TypeAST): Type {
    var root = implicitPath
    while (true) {
      val result = map[QN(root + explicitPath.path, explicitPath.name)]
      if (result != null) return result
      if (root.isEmpty()) throw Exception("No such type: ${explicitPath} in ${implicitPath}. Available types: ${map.keys}")
      root = root.dropLast(1)
    }
  }

  fun qualify(implicitPath: List<String>, explicitPath: TypeAST): QN {
    var root = implicitPath
    while (true) {
      val qn = QN(root + explicitPath.path, explicitPath.name)
      if (map.containsKey(qn)) return qn
      if (root.isEmpty()) throw Exception("No such type: ${explicitPath} in ${implicitPath}. Available types: ${map.keys}")
      root = root.dropLast(1)
    }
  }

  fun ir(implicitPath: List<String>, typeName: String): String {
    var root = implicitPath
    while (true) {
      val result = map[QN(root, typeName)]
      if (result != null) return "%\"${root.joinToString(".")}.${result.friendly()}\""
      if (root.isEmpty()) throw Exception("No such type: ${typeName} in ${implicitPath}. Available types: ${map.keys}")
      root = root.dropLast(1)
    }
  }

  fun ir(implicitPath: List<String>, explicitPath: TypeAST): String {
    var root = implicitPath
    while (true) {
      val result = map[QN(root + explicitPath.path, explicitPath.name)]
      if (result != null) return "%\"${(root + explicitPath.path).joinToString(".")}.${result.friendly()}\""
      if (root.isEmpty()) throw Exception("No such type: ${explicitPath} in ${implicitPath}. Available types: ${map.keys}")
      root = root.dropLast(1)
    }
  }

  fun ir(qn: QN): String {
    val result = map[qn]
    if (result != null) return "%\"${qn.path.joinToString(".")}.${result.friendly()}\""
    throw Exception("No such type: ${qn}. Available types: ${map.keys}")
  }

  fun update(other: TypeMap, prefix: List<String>?) {
    other.map.forEach { this[it.key.prefix(prefix)] = it.value }
  }

  // Like update(), but non-destructive
  fun merge(other: TypeMap, prefix: List<String>?): TypeMap {
    val ret = TypeMap(map.toMutableMap())
    ret.update(other, prefix)
    return ret
  }

  fun update_aliases(other: Map<TypeAST, TypeAST>, prefix: List<String>) {
    other.forEach { (alias, real) ->
      val aliasQN = QN(prefix + alias.path, alias.name)
      if (map.containsKey(aliasQN)) {
        throw Exception("Alias redefines ${aliasQN}")
      }
      map[aliasQN] = this[prefix, real]
    }
  }

  fun text(): String {
    return buildString {
      map.forEach {
        append(it.key)
        append(": ")
        append(it.value.friendly())
        append("\n")
      }
    }
  }

  fun forEach(lambda: (Map.Entry<QN, Type>) -> Unit) = map.forEach(lambda)
}
